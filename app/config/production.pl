use File::Spec;
use File::Basename qw(dirname);
my $basedir = File::Spec->rel2abs(File::Spec->catdir(dirname(__FILE__), '..'));
my $dbpath = File::Spec->catfile($basedir, 'db', 'production.db');
+{
    'DBI' => [ 'dbi:mysql:database=resonote;host=localhost', 'root', '', { mysql_enable_utf8 => 1 } ],
    'Facebook::Graph' => {
        app_id => '888503367876205',
        secret => '9c806bccc524be29b769492ee6c8e2d5',
        postback => 'http://resonote.dox.gaiax.com/callback',
    },
};
