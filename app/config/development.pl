use File::Spec;
use File::Basename qw(dirname);
my $basedir = File::Spec->rel2abs(File::Spec->catdir(dirname(__FILE__), '..'));
my $dbpath = File::Spec->catfile($basedir, 'db', 'development.db');
+{
    'DBI' => [ 'dbi:mysql:database=resonote;host=localhost', 'root', '', { mysql_enable_utf8 => 1 } ],
    'Facebook::Graph' => {
        app_id => '1582372478681970',
        secret => 'eb48195576e874ed41635f5dcbd3dc36',
        postback => 'http://192.168.33.10/callback',
    },
};
