SET foreign_key_checks=0;

--
-- Table: `users`
--
CREATE TABLE `users` (
  `id` INTEGER unsigned NOT NULL auto_increment,
  `facebook_id` VARCHAR(255) DEFAULT NULL,
  `email` VARCHAR(255) DEFAULT NULL,
  `password` VARCHAR(255) DEFAULT NULL,
  `gender` VARCHAR(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `last_logined_on` datetime DEFAULT NULL,
  `deleted` TINYINT NOT NULL DEFAULT 0,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  UNIQUE INDEX `facebook_id_uniq` (`facebook_id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;


--
-- Table: `notes`
--
CREATE TABLE `notes` (
  `id` INTEGER unsigned NOT NULL auto_increment,
  `user_id` INTEGER unsigned NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `reference` VARCHAR(255) DEFAULT NULL,
  `deleted` TINYINT NOT NULL DEFAULT 0,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

--
-- Table: `comments`
--
CREATE TABLE `comments` (
  `id` INTEGER unsigned NOT NULL auto_increment,
  `user_id` INTEGER unsigned NOT NULL,
  `note_id` INTEGER unsigned NOT NULL,
  `content` TEXT NOT NULL,
  `deleted` TINYINT NOT NULL DEFAULT 0,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX note_user_index (`note_id`, `user_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

--
-- Table: `tags`
--
CREATE TABLE `tags` (
  `id` INTEGER unsigned NOT NULL auto_increment,
  `name` VARCHAR(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX unique_name (name)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

--
-- Table: `tag_map`
--
CREATE TABLE `tag_map` (
  `id` INTEGER unsigned NOT NULL auto_increment,
  `user_id` INTEGER unsigned NOT NULL,
  `note_id` INTEGER unsigned NOT NULL,
  `tag_id` INTEGER unsigned NOT NULL,
  `locked` TINYINT NOT NULL DEFAULT 0,
  `created_by` INTEGER unsigned NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` INTEGER unsigned NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  INDEX tag_id_index (`tag_id`),
  UNIQUE INDEX note_tag_index (`note_id`, `tag_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

--
-- Table: `stocks`
--
CREATE TABLE `stocks` (
  `id` INTEGER unsigned NOT NULL auto_increment,
  `user_id` INTEGER unsigned NOT NULL,
  `note_id` INTEGER unsigned NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX user_note_index (`user_id`, `note_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

--
-- Table: `likes`
--
CREATE TABLE `likes` (
  `id` INTEGER unsigned NOT NULL auto_increment,
  `user_id` INTEGER unsigned NOT NULL,
  `note_id` INTEGER unsigned NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX user_note_index (`user_id`, `note_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;
