$(document).ready(function(){
    var MAINCOLOR        = "#0e90a9";
    var MAINCOLOR_LIGHT1 = "#96d4e0";
    var MAINCOLOR_LIGHT2 = "#f1fcfe";
    var MAINCOLOR_DARK1  = "#037388";
    var MAINCOLOR_DARK2  = "#025a6a";
    var SUBCOLOR         = "#ffc33f";
    var SUBCOLOR_LIGHT1  = "#ffd882";
    var SUBCOLOR_LIGHT2  = "#fffcf5";
    var SUBCOLOR_DARK1   = "#f3ad0c";
    var FACEBOOK_COLOR1  = "#3b5998";
    var FACEBOOK_COLOR2  = "#19294c";

    var SPEED = 200;

    $("#main-visual .btn, .begin-area .btn").hover(function() {
        $(this).stop().animate(
            { "backgroundColor" : MAINCOLOR_DARK2 }, SPEED);

        $(this).find("i").stop().animate(
            {
                "left" : "80%",
                "opacity" : "1" }, SPEED);
    },function() {
        $(this).stop().animate(
            { "backgroundColor" : MAINCOLOR }, SPEED);

        $(this).find("i").stop().animate(
            {
                "left" : "130%",
                "opacity" : "0" }, SPEED);
    });
    $("#main-visual .btn.login, .begin-area .btn.login").hover(function() {
        $(this).stop().animate(
            { "backgroundColor" : FACEBOOK_COLOR2 }, SPEED);

        $(this).find("i").stop().animate(
            {
                "left" : "80%",
                "opacity" : "1" }, SPEED);
    },function() {
        $(this).stop().animate(
            { "backgroundColor" : FACEBOOK_COLOR1 }, SPEED);

        $(this).find("i").stop().animate(
            {
                "left" : "130%",
                "opacity" : "0" }, SPEED);
    });

    $(".notelist-area .panel .panel-body").hover(function() {
	$(this).stop().animate(
	    { "backgroundColor" : MAINCOLOR_LIGHT1 }, SPEED);
    },function() {
	$(this).stop().animate(
	    { "backgroundColor" : MAINCOLOR_LIGHT2 }, SPEED);
    });
    $(".notelist-area .panel.my .panel-body").hover(function() {
	$(this).stop().animate(
	    { "backgroundColor" : SUBCOLOR_LIGHT1 } , SPEED);
    },function() {
	$(this).stop().animate(
	    { "backgroundColor" : SUBCOLOR_LIGHT2 }, SPEED);
    });

    $('#add-tag').click(function() {
        $(this).hide();
        $('#add-tag-form').show().focus();
    });

    $('#add-tag-form').focusout(function() {
        var tag_name = $(this).val();
        var note_id = $(this).data('note_id');
        if (tag_name) {
            $.ajax({
                type: "POST",
                url: "/tags/add",
                data: {
                    'XSRF-TOKEN': $.cookie('XSRF-TOKEN'),
                    'note_id' : note_id,
                    'tag_name' : tag_name,
                },
                success: function(msg) {
                    console.log(msg);
                    $('#add-tag').before(msg);
                },
                error: function(msg) {
                    console.log(msg);
                }
            });
        }
        $('#add-tag-form').val('').hide();
        $('#add-tag').show();
    });

    $('.del-comment').click(function() {
        var self = this;
        if (confirm("コメントを削除します。よろしいですか？（この操作は取り消せません）")) {
            $.ajax({
                type: "POST",
                url: "/notes/" + $(this).data('note_id') + "/comment/delete",
                data: {
                    'XSRF-TOKEN': $.cookie('XSRF-TOKEN'),
                    'comment_id' : $(this).data('comment_id'),
                },
                success: function(msg) {
                    console.log(msg);
                    if (msg.result) {
                        $(self).parent().parent().remove();
                    }
                },
                error: function(msg) {
                    console.log(msg);
                }
            });
        }
    });
});

$(document).on('click', '.like', function(){
    var NOTE_ID = $(this).val();
    console.log(NOTE_ID);

    $.ajax({
    type: "POST",
    url: "/like/" + NOTE_ID,
    data: {
        'XSRF-TOKEN': $.cookie('XSRF-TOKEN'),
        'note_id' : NOTE_ID
    },
    success: function(msg) {
        console.log("success: " + msg);
        var btn = $(".like");
        btn.removeClass('like').addClass('liked');
        btn.html('<i class="fa fa-check"></i> なるほど');
        var cnt = $(".like-count");
        cnt.html(parseInt(cnt.text()) + 1);
    },
    error: function(msg) {
        console.log("error: " + msg);
    }
    });
});

$(document).on({
    click: function(){
        var NOTE_ID = $(this).val();
        console.log(NOTE_ID);

        $.ajax({
        type: "POST",
        url: "/unlike/" + NOTE_ID,
        data: {
            'XSRF-TOKEN': $.cookie('XSRF-TOKEN'),
            'note_id' : NOTE_ID
        },
        success: function(msg) {
            console.log("success: " + msg);
            var btn = $(".liked");
            btn.removeClass('liked').addClass('like');
            btn.html('<i class="fa fa-exclamation"></i> なるほど');
            var cnt = $(".like-count");
            cnt.html(parseInt(cnt.text()) - 1);
        },
        error: function(msg) {
            console.log("error: " + msg);
        }
        });
    },
    mouseenter: function() {
        $(this).html('<i class="fa fa-close"></i> 取り消す');
    },
    mouseleave: function() {
        $(this).html('<i class="fa fa-check"></i> なるほど');
    },
}, '.liked');

$(document).on('click', '.stock', function(){
    var NOTE_ID = $(this).val();
    console.log(NOTE_ID);

    $.ajax({
	type: "POST",
	url: "/stock/" + NOTE_ID,
	data: {
	    'XSRF-TOKEN': $.cookie('XSRF-TOKEN'),
	    'note_id' : NOTE_ID
	},
	success: function(msg) {
	    console.log("success: " + msg);
        var btn = $(".stock");
        btn.removeClass('stock').addClass('stocked');
        btn.html('<i class="fa fa-check"></i> ストック');
        var cnt = $(".stock-count");
        cnt.html(parseInt(cnt.text()) + 1);
   	},
   	error: function(msg) {
   	    console.log("error: " + msg);
   	}
    });
});

$(document).on({
    click: function(){
        var NOTE_ID = $(this).val();
        console.log(NOTE_ID);

        $.ajax({
	    type: "POST",
	    url: "/unstock/" + NOTE_ID,
	    data: {
	        'XSRF-TOKEN': $.cookie('XSRF-TOKEN'),
	        'note_id' : NOTE_ID
	    },
	    success: function(msg) {
	        console.log("success: " + msg);
            var btn = $(".stocked");
            btn.removeClass('stocked').addClass('stock');
            btn.html('<i class="fa fa-download"></i> ストック');
            var cnt = $(".stock-count");
            cnt.html(parseInt(cnt.text()) - 1);
   	    },
   	    error: function(msg) {
   	        console.log("error: " + msg);
            console.log("a");
   	    }
        });
    },
    mouseenter: function() {
        $(this).html('<i class="fa fa-close"></i> 解除する');
    },
    mouseleave: function() {
        $(this).html('<i class="fa fa-check"></i> ストック');
    },
}, '.stocked');

$(document).on({
    mouseenter: function() {
	$(this).stop().animate({
	    "top" : "-3px",
	    "opacity" : "0.8"
        }, 200);
    },
    mouseleave: function() {
	$(this).stop().animate({
	    "top" : "0",
	    "opacity" : "1"
        }, 200);
    },
}, '.taglist .tag');

$(document).on('click', '.tag .delete', function() {
    var tag = $(this).parent();
    if (confirm($(this).data('tag_name') + "を削除します。よろしいですか？")) {
        $.ajax({
            type: "POST",
            url: "/tags/delete",
            data: {
                'XSRF-TOKEN': $.cookie('XSRF-TOKEN'),
                'note_id' : $(this).data('note_id'),
                'tag_id' : $(this).data('tag_id'),
            },
            success: function(msg) {
                console.log("success: " + msg);
                tag.remove();
            },
            error: function(msg) {
                console.log("error: " + msg);
            }
        });
    }
});
