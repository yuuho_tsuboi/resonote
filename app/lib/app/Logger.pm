package app::Logger;
use parent 'Log::Pony';

use Try::Tiny;

sub log {
    my $self = shift;

    my @args = do {
        my %args = @_;
        if (exists $args{level} and exists $args{message}) {
            my $level   = delete $args{level};
            my $message = delete $args{message};
            ($level, $message, %args);
        } else {
            @_;
        }
    };

    local $Log::Pony::TRACE_LEVEL = $Log::Pony::TRACE_LEVEL + 1;
    $self->SUPER::log(@args);
}

sub process {
    my ($self, $level, $message) = @_;
    my $time = $self->time();
    if ($self->color) {
        $message = $self->colorize($level, $message);
    }
    printf STDERR "%s [%s] %s\n", $time, lc($level), $message;
}

1;
