package app::Core;
use strict;
use warnings;
use utf8;

use parent 'Exporter';

use Path::Tiny;
use app::DB;
use app::DB::Schema;
use app::Logger;
use Facebook::Graph;

our @EXPORT = qw/ db logger txn_scope current_time config /;
our $DB;
our $FB;
our $LOGGER;
our $CONFIG;

sub db {
    if (! $DB) {
        my $conf   = app::Core->config->{DBI} or die "Missing configuration about DBI";
        my $schema = app::DB::Schema->instance;
        $DB = app::DB->new(
            schema        => $schema,
            connect_info  => [@$conf],
            on_connect_do => [
                'SET SESSION sql_mode=STRICT_TRANS_TABLES;',
            ],
        );
    }
    $DB;
}

sub fb {
    if (! $FB) {
        my $conf = app::Core->config->{'Facebook::Graph'} or die "missing configuration for 'Facebook::Graph'";
        $FB = Facebook::Graph->new(
            app_id      => $conf->{app_id},
            secret      => $conf->{secret},
            postback    => $conf->{postback},
        );
    }
    $FB;
}

sub config {
    if (! $CONFIG) {
        my $config_path = path( 'config', lc( $ENV{PLACK_ENV} || 'development' ).'.pl' );
        $CONFIG = do $config_path or die "$config_path: $!";
    }
    $CONFIG;
}

sub logger {
    $LOGGER ||= app::Logger->new(
        color     => 1,
        log_level => ($ENV{DOX_LOG_LEVEL} || ($ENV{PLACK_ENV} eq 'production' ? 'info' : 'debug')),
    );
}

sub txn_scope { db()->txn_scope }

sub current_time { scalar localtime }

1;
