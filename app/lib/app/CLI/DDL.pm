package app::CLI::DDL;
use strict;
use warnings;
use utf8;
use GitDDL::Migrator;
use Path::Tiny;
use Try::Tiny;

my $config_path = path( 'config', lc( $ENV{DAIKU_ENV} || 'development' ).'.pl' );
my $config      = do $config_path or die "$config_path: $!";
my $gd = GitDDL::Migrator->new(
    work_tree => path('../'),
    ddl_file  => path('app/sql', 'mysql.sql'),
    dsn       => $config->{DBI},
);

sub check_version {
    if ( $gd->check_version ) {
        print "database is latest\n";
        return 1;
    } else {
        print "database is not latest\n";
        return 0;
    }
}

sub upgrade {
    try {
        $gd->upgrade_database;
    } catch {
        my $e = shift;
        print $e;
    };
}

sub diff {
    print $gd->diff
}

sub deploy {
    try {
        $gd->deploy;
    } catch {
        my $e = shift;
        print $e;
    };
}


1;
