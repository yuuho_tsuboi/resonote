package app::CLI::BuildSchema;
use strict;
use warnings;
use DBI;
use Path::Tiny;
use Teng::Schema::Dumper;
use Test::mysqld;

sub run {
    my $MYSQLD = Test::mysqld->new( my_cnf => { 'skip-networking' => '' })
        or die $Test::mysqld::errstr;
    my $dbh = DBI->connect($MYSQLD->dsn);

    my $file_name = 'sql/mysql.sql';
    my $source    = path($file_name)->slurp_utf8;

    for my $stmt (split /;/, $source) {
        next unless $stmt =~ /\S/;
        $dbh->do($stmt) or die $dbh->errstr;
    }

    my $schema_class = 'lib/app/DB/Schema.pm'; # 出力先となるSchemaクラス
    open my $fh, '>', $schema_class or die "$schema_class \: $!";
    print $fh Teng::Schema::Dumper->dump(
        dbh => $dbh,
        namespace      => 'app::DB',
        base_row_class => 'app::DB::Row',
        inflate        => { },
    );
    close $fh;
}

1;
