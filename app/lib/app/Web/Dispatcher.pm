package app::Web::Dispatcher;
use strict;
use warnings;
use utf8;
use Amon2::Web::Dispatcher::RouterBoom;
use Module::Find;

useall 'app::Web::Controller';
base   'app::Web::Controller';

get '/' => 'Root#index';
post '/logout' => 'Login#logout';
get '/callback' => 'Login#callback';

get '/mylist' => 'Mylist#mylist';

get '/notes/create' => 'Note#note_creation_form';
post '/notes/create' => 'Note#create_note';
get '/notes/:id' => 'Note#note_detail';
post '/notes/:id/delete' => 'Note#delete_note';
post '/notes/:id/comment' => 'CommentNote#comment';
post '/notes/:id/comment/delete' => 'CommentNote#delete';

get '/notes' => 'Notes#notes_list';

post '/like/:id' => 'LikeNote#like_note';
post '/unlike/:id' => 'LikeNote#unlike_note';

post '/stock/:id' => 'StockNote#stock_note';
post '/unstock/:id' => 'StockNote#unstock_note';

post '/tags/add' => 'Tags#add';
post '/tags/delete' => 'Tags#delete';

1;
