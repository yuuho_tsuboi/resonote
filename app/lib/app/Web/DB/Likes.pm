package app::Web::DB::Likes;

use app::Web::Util::DBHelper;

sub unlike {
    my ($class, $c, $note_id) = @_;
    $c->db->delete('likes', {
        user_id => $c->user_id,
	note_id => $note_id,
    });
}

sub like{
    my($class, $c, $note_id) = @_;
    $c->db->insert('likes', {
        user_id => $c->user_id(),
	note_id => $note_id,
	created_on => app::Web::Util::DBHelper->current_time_for_db(),
	updated_on => app::Web::Util::DBHelper->current_time_for_db(),
    });
}

sub number_of_like{
    my ($class, $c, $note_id) = @_;
    my @likes = $c->db->search('likes', {
        note_id => $note_id,
    });
    my $num = @likes;
    return $num;
}

sub liked_by_user_logined{
    my ($class, $c, $note_id) = @_;
    my $like = $c->db->single('likes', {
        note_id => $note_id,
        user_id => $c->user_id,
    });
    return defined $like ? 1 : 0;
}
1;
