package app::Web::DB::TagMap;

use app::Web::Util::DBHelper;
use app::Web::DB::Tags;
no warnings 'redefine';
use Log::Minimal;
use Data::Dumper;

sub add {
    my ($class, $c, $note_id, $tag_name) = @_;
    if (_can_mapped_name_as_tag_to_note($c, $note_id, $tag_name)){
        my $tag = app::Web::DB::Tags->create_tag($c, $tag_name);
        _insert($c, $note_id, $tag->id);
    }
}

sub tag_map_of_note {
    my($class, $c, $note) = @_;
    return $c->db->search('tag_map', {
        note_id => $note->id,
    });
}

sub put_note_to_tag {
    my($class, $c, $note, $tag) = @_;
    if (_can_mapped_name_as_tag_to_note($c, $note->id, $tag->name)) {
        _insert($c, $note->id, $tag->id);
    }
}

sub _insert {
    my ($c, $note_id, $tag_id) = @_;
    $c->db->insert('tag_map', {
        user_id => $c->user_id,
        note_id => $note_id,
        tag_id => $tag_id,
	locked => 0,
	created_by => $c->user_id,
	updated_by => $c->user_id,
	created_on => app::Web::Util::DBHelper->current_time_for_db(),
	updated_on => app::Web::Util::DBHelper->current_time_for_db(),
    });
}

sub tag_map_of_tags {
    my ($class, $c, $tags) = @_;

    my @maps;
    for my $tag (@$tags) {
        if (@maps) {
            @maps = $c->db->search('tag_map', {
                tag_id => $tag->id,
                note_id => [map {$_->note_id;} @maps] ,
            });
        } else {
            @maps = $c->db->search('tag_map', {
                tag_id => $tag->id,
            });
        }
    }

    return @maps;
}

sub get_tag_map_for_recommend {
    my ($class, $c, $notes, $limit) = @_;

    if (@$notes) {
        return $c->db->search_named(
            q{SELECT tag_id, count(*) AS count FROM tag_map WHERE note_id IN :note_ids GROUP BY tag_id ORDER BY count DESC LIMIT :limit},
            {
                note_ids => [map {$_->id;} @$notes],
                limit => $limit,
            }
        );
    } else {
        return $c->db->search_named(
            q{SELECT tag_id, count(*) AS count FROM tag_map GROUP BY tag_id ORDER BY count DESC LIMIT :limit},
            {
                limit => $limit,
            }
        );
    }
}

sub _can_mapped_name_as_tag_to_note {
    my ($c, $note_id, $tag_name) = @_;
    my @tag_map = $c->db->search('tag_map', {
        note_id => $note_id,
    });

    for my $tag_map (@tag_map){
        my $tag = $c->db->single('tags', {
            id => $tag_map->tag_id,
        });
        if($tag->name eq $tag_name) {
            return 0;	
	}
    }

    return 1;
}

1;
