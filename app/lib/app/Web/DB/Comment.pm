package app::Web::DB::Comment;

use app::Web::Util::DBHelper;

sub comment_to_note {
    my ($class, $c, $note_id) = @_;
    return $c->db->insert('comments', {
        user_id => $c->user_id,
        note_id => $note_id,
        content => $c->req->parameters->{content},
	created_on => app::Web::Util::DBHelper->current_time_for_db(),
        updated_on => app::Web::Util::DBHelper->current_time_for_db(),
    });
}

sub delete {
    my ($class, $c, $comment_id) = @_;
    my $comment = $c->db->single('comments', {
        id => $comment_id,
    });

    if ($comment && $comment->user_id == $c->user_id) {
        $comment->update({
            deleted => 1,
            updated_on => app::Web::Util::DBHelper->current_time_for_db(),
        });
        return 1;
    }
    return 0;
}

1;
