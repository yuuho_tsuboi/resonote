package app::Web::DB::Users;

use app::Web::Util::DBHelper;

sub fb_user {
    my ($class, $c) = @_;
    return $c->db->single('users', {
        facebook_id => $c->fb->fetch('me')->{id}
    });
}

sub regist_user_from_facebook {
    my ($class, $c) = @_;
    return $c->db->insert('users', {
        facebook_id => $c->fb->fetch('me')->{id},
	created_on => app::Web::Util::DBHelper->current_time_for_db(),
	updated_on => app::Web::Util::DBHelper->current_time_for_db(),
    });
}

sub facebook_user {
    my ($class, $c) = @_;
    return $c->db->single('users', {
        facebook_id => $c->fb->fetch('me')->{id}
    });
}

sub regist{
    my ($class, $c) = @_;
    return $c->db->insert('users', {
        email => $c->req->parameters->{email},
	password => $c->req->parameters->{password},
        gender => $c->req->parameters->{gender},
	birthday => $c->req->parameters->{birthday},
	last_logined_on => app::Web::Util::DBHelper->current_time_for_db(),
	created_on => app::Web::Util::DBHelper->current_time_for_db(),
	updated_on => app::Web::Util::DBHelper->current_time_for_db(),
    });
}

1;
