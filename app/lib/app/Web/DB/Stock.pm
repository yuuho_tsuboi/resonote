package app::Web::DB::Stock;

sub stocks_of_logined_user {
    my ($class, $c) = @_;
    return $c->db->search('stocks', {
        user_id => $c->user_id,
    });
}

sub stocked_by_user_logined {
    my($class, $c, $note_id) = @_;
    my $stock = $c->db->single('stocks', {
        note_id => $note_id,
	user_id => $c->user_id,
    });
    return defined $stock ? 1 : 0;
}

sub delete_stock {
    my ($class, $c, $note_id) = @_;
    $c->db->delete('stocks', {
        user_id => $c->user_id,
	note_id => $note_id,
    });
}

sub create_stock {
    my ($class, $c, $note_id) = @_;
    my $stock = $c->db->insert('stocks', {
        user_id => $c->user_id,
	note_id => $note_id,
	created_on => app::Web::Util::DBHelper->current_time_for_db(),
	updated_on => app::Web::Util::DBHelper->current_time_for_db(),
    });
}

sub number_of_stock {
    my ($class, $c, $note_id) = @_;
    my @stocks = $c->db->search('stocks', {
        note_id => $note_id,
    });
    my $num = @stocks;
    return $num;
}
1;
