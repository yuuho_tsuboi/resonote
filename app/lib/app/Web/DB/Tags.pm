package app::Web::DB::Tags;

use app::Web::Util::DBHelper;
use app::Web::DB::TagMap;

sub create_tag {
    my($class, $c, $name) = @_;
    if(_is_existing_tag($c, $name)){
        return $c->db->single('tags', {
            name => $name,
        });
    }

    return $c->db->insert('tags', {
        name => $name,
        created_on => app::Web::Util::DBHelper->current_time_for_db(),
        updated_on => app::Web::Util::DBHelper->current_time_for_db(),
    });
}

sub _is_existing_tag {
    my ($c, $name) = @_;
    my $tag = $c->db->single('tags', {
       name => $name,
    });
    return defined $tag ? 1 : 0;
}

sub get_tags_for_recommend {
    my ($class, $c, $notes, $limit) = @_;
    my @map = app::Web::DB::TagMap->get_tag_map_for_recommend($c, $notes, $limit);
    return $class->get_tags_by_id($c, [map {$_->tag_id;} @map]);
}

sub get_tags_by_id {
    my ($class, $c, $id) = @_;
    return $c->db->search('tags', {
        id => $id,
    });
}

sub get_tags_by_keywords {
    my ($class, $c, $keywords) = @_;
    return $c->db->search('tags', {
        name => [@$keywords],
    });
}

1;
