package app::Web::DB::Notes;

use app::Web::DB::Stock;
use app::Web::Util::DBHelper;
use app::Web::DB::Tags;
use app::Web::DB::TagMap;
use SQL::QueryMaker;
use Try::Tiny;

sub get_notes_for_mylist {
    my ($class, $c, $page) = @_;
    my @stocks = app::Web::DB::Stock->stocks_of_logined_user($c);
    return $c->db->search_with_pager(
        'notes',
        sql_and([ sql_eq(deleted => 0), sql_or({ user_id => $c->user_id, id => sql_in([map {$_->note_id} @stocks])}) ]),
        {page => $page, rows => 10}
    );
}

sub get_notes_for_search {
    my($class, $c, $keywords, $page) = @_;
    my @tags = app::Web::DB::Tags->get_tags_by_keywords($c, $keywords);
    my @maps = app::Web::DB::TagMap->tag_map_of_tags($c, \@tags);
    return $c->db->search_with_pager(
        'notes',
        {
            id => [map {$_->note_id;} @maps],
            deleted => 0,
        },
        {page => $page, rows => 10}
    );
}

sub create_note{
    my($class, $c) = @_;
    return $c->db->insert('notes', {
        title => $c->req->parameters->{'title'},
        description => $c->req->parameters->{'description'},
	user_id => $c->user_id,
	reference => $c->req->parameters->{'reference'},
	created_on => app::Web::Util::DBHelper->current_time_for_db(),
	updated_on => app::Web::Util::DBHelper->current_time_for_db(),
    });
}

sub note{
    my($class, $c, $id) = @_;
    return $c->db->single('notes', {
        id => $id,
	deleted => 0,
    });
}

sub logical_delete_note {
    my ($class, $c, $note_id) = @_;

    my $txn = $c->db->txn_scope;
    try {
        my $note = $c->db->single('notes', {
            id => $note_id,
            deleted => 0,
        });

        die 'Can\'t delete this note. id: '.$note_id  unless $note->is_author($c->user_id);
        $note->update({
            deleted => 1
        });

        $c->db->delete('tag_map', {
            note_id => $note_id,
        });

        $txn->commit;
        return 1;
    } catch {
        $txn->rollback;
        Carp::croak $_;
    };
}

sub _can_delete {
    my ($class, $c, $note_id) = @_;
    return $class->note($c, $note_id)->is_author($c->user_id) ? 1 : 0;
}

sub update {
    my ($class, $c, $note_id) = @_;
    $c->db->update('notes', {
        title => $c->req->parameters->{title},
        description =>$c->req->parameters->{description},
	reference => $c->req->parameters->{reference},
	updeated_on => app::Web::Util::DBHelper->current_time_for_db(),
    },
    {
        id => $note_id,
    });
}

1;
