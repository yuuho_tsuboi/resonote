package app::Web::Controller::Mylist;
use strict;
use warnings;
use utf8;

use app::Web::Util::DBHelper;
use app::Web::DB::Notes;

sub mylist {
    my ($class, $c) = @_;
    my $page = $c->req->parameters->{page} || 1;
    my ($notes, $pager) = app::Web::DB::Notes->get_notes_for_mylist($c, $page);
    return $c->render('mylist.tx', {
         notes => $notes,
         pager => $pager,
    });
};

1;
