package app::Web::Controller::StockNote;
use strict;
use warnings;
use utf8;

use app::Web::Util::DBHelper;
use app::Web::DB::Stock;

sub stock_note {
    my ($class, $c, $args) = @_;
    my $note_id = $args->{id};
    app::Web::DB::Stock->create_stock($c, $note_id);
    return $c->render('index.tx');
}

sub unstock_note {
    my ($class, $c, $args) = @_;
    my $note_id = $args->{id};
    app::Web::DB::Stock->delete_stock($c, $note_id);
    return $c->render('index.tx');
}

1;
