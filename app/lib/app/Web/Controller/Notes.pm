package app::Web::Controller::Notes;

use app::Web::DB::Notes;

sub notes_list {
    my($class, $c) = @_;
    my $page = $c->req->parameters->{page} || 1;
    my $keyword = $c->req->parameters->{keyword};
    my @keywords = split(' ', $keyword);

    my ($notes, $pager);
    if (@keywords) {
        ($notes, $pager) = app::Web::DB::Notes->get_notes_for_search($c, \@keywords, $page);
    }
    my @tags = app::Web::DB::Tags->get_tags_for_recommend($c, $notes, 10);

    return $c->render('notes.tx', {
        notes => $notes,
        pager => $pager,
        recommend_tags => \@tags,
        keyword => $keyword,
    });
}

1;
