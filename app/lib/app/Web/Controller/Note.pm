package app::Web::Controller::Note;

use Log::Minimal;
use Data::Dumper;

use app::Web::DB::Notes;
use app::Web::DB::Tags;
use app::Web::DB::Likes;
use app::Web::DB::Comment;

sub note_creation_form {
    my ($class, $c) = @_;
    return $c->render('new.tx');
}

sub create_note {
    my ($class, $c) = @_;
    my $note = app::Web::DB::Notes->create_note($c);
    my @name = _tag_names($c);
    for my $name (@name) {
        my $tag = app::Web::DB::Tags->create_tag($c, $name);
        app::Web::DB::TagMap->put_note_to_tag($c, $note, $tag);
    }
    my $note_id = $note->id;
    return $c->redirect("/notes/$note_id");
}

sub _tag_names {
    my ($c) = @_;
    my $tags = $c->req->parameters->{'name'};
    $tags =~ s/,\s+/,/g;
    $tags =~ s/　/\s/g;
    $tags =~ s/\s/,/g;
    return split(/,/, $tags);
}

sub note_detail {
    my ($class, $c, $args) = @_;
    my $id = $args->{id};
    my $note = app::Web::DB::Notes->note($c, $id);
    return $c->render('note.tx', {
        note => $note,
	liked => app::Web::DB::Likes->liked_by_user_logined($c, $id),
        stocked => app::Web::DB::Stock->stocked_by_user_logined($c, $id),
        number_of_like => app::Web::DB::Likes->number_of_like($c, $id),
        number_of_stock => app::Web::DB::Stock->number_of_stock($c, $id),
    });
}

sub delete_note {
    my($class, $c, $args) = @_;
    my $note_id = $args->{id};
    app::Web::DB::Notes->logical_delete_note($c, $note_id);
    return $c->redirect('/mylist');
}

1;
