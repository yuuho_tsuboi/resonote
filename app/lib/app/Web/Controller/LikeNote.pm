package app::Web::Controller::LikeNote;

use app::Web::DB::Likes;

sub like_note {
    my ($class, $c, $args) = @_;
    my $note_id = $args->{id};
    app::Web::DB::Likes->like($c, $note_id);
    return $c->render('index.tx');
}

sub unlike_note {
    my ($class, $c, $args) = @_;
    my $note_id = $args->{id};
    app::Web::DB::Likes->unlike($c, $note_id);
    return $c->render('index.tx');
}

1;
