package app::Web::Controller::Login;
use strict;
use warnings;
use utf8;

use app::Web::DB::Users;

sub logout {
    my ($class, $c) = @_;
    $c->session->expire;
    return $c->redirect('/');
}

sub callback {
    my ($class, $c) = @_;
    $c->request_access_token;
    if(_is_new_fb_user($c)){
        app::Web::DB::Users->regist_user_from_facebook($c);
    }
    $c->set_fb_user_id_to_session;
    return $c->redirect('/mylist');
}

sub _is_new_fb_user {
    my ($c) = @_;
    if($c->logined){
        return 0;
    }
    my $user =  app::Web::DB::Users->fb_user($c);
    return !defined $user ? 1 : 0;
}

1;
