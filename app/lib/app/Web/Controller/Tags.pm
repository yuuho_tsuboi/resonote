package app::Web::Controller::Tags;
use strict;
use warnings;
use utf8;

use app::Web::Util::DBHelper;
use app::Web::DB::TagMap;

sub add {
    my ($class, $c, $args) = @_;
    my $note_id = $c->req->parameters->{note_id};
    my $tag_name = $c->req->parameters->{tag_name};
    my $tag_id = app::Web::DB::TagMap->add($c, $note_id, $tag_name)->tag_id;
    return $c->render('notes/tag.tx', {tag_id => $tag_id, tag_name => $tag_name, note_id => $note_id});
}

sub delete {
    my ($class, $c, $args) = @_;
    my $note_id = $c->req->parameters->{note_id};
    my $tag_id = $c->req->parameters->{tag_id};

    $c->db->delete('tag_map', {
        note_id => $note_id,
        tag_id => $tag_id,
    });
    return $c->render_json({});
}

1;
