package app::Web::Controller::CommentNote;

use app::Web::DB::Comment;

sub comment {
    my ($class, $c, $args) = @_;
    my $note_id = $args->{id};
    my $comment = app::Web::DB::Comment->comment_to_note($c, $note_id);
    return $c->redirect("/notes/$note_id");
}

sub delete {
    my ($class, $c, $args) = @_;
    my $comment_id = $c->req->parameters->{comment_id};
    my $result = app::Web::DB::Comment->delete($c, $comment_id);
    return $c->render_json({result => $result});
}

1;
