package app::Web::Controller::Root;
use strict;
use warnings;
use utf8;

use Try::Tiny;

sub index {
    my ($class, $c) = @_;

    if($c->logined){
	return $c->redirect(
	    $c->fb->authorize->uri_as_string,
	);
    }else{
	return $c->render('index.tx', {
            'fb_authrize_url' => $c->fb->authorize->uri_as_string,
        });
    }
}

1;
