package app::Web::Util::DBHelper;

use DateTime;
use DateTime::Format::MySQL;

sub current_time_for_db{
    my($class) = @_;
    my $dt = DateTime->now( time_zone=>'local' );
    return DateTime::Format::MySQL->format_datetime($dt);
}

1;
