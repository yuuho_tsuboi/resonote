package app::DB::Row::Notes;
use strict;
use warnings;
use parent 'app::DB::Row';

sub to_tags {
    my $self = shift;
    my @map = $self->handle->search(tag_map => +{ note_id => $self->id });
    my @result = $self->handle->search(tags => +{ id => [map {$_->tag_id;} @map] });
    return \@result;
}

sub to_comments {
    my $self = shift;
    my @comments = $self->handle->search(comments => +{
        note_id => $self->id,
        deleted => 0,
    });
    return \@comments;
}

sub is_author {
    my ($self, $user_id) = @_;
    return ($self->user_id == $user_id);
}

sub is_stocked {
    my ($self, $user_id) = @_;
    my $stock = $self->handle->single('stocks',{
        note_id => $self->id,
        user_id => $user_id,
    });
    return defined $stock ? 1 : 0;
}

1;
