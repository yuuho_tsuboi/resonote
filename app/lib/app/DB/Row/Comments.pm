package app::DB::Row::Comments;
use strict;
use warnings;
use parent 'app::DB::Row';

sub is_created_by {
    my ($self, $user_id) = @_;
    return ($self->user_id == $user_id);
}

1;
