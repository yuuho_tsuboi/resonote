package app::DB::Schema;
use strict;
use warnings;
use Teng::Schema::Declare;
base_row_class 'app::DB::Row';
table {
    name 'comments';
    pk 'id';
    columns (
        {name => 'id', type => 4},
        {name => 'user_id', type => 4},
        {name => 'note_id', type => 4},
        {name => 'content', type => 12},
        {name => 'deleted', type => 4},
        {name => 'created_on', type => 11},
        {name => 'updated_on', type => 11},
    );
};

table {
    name 'likes';
    pk 'id';
    columns (
        {name => 'id', type => 4},
        {name => 'user_id', type => 4},
        {name => 'note_id', type => 4},
        {name => 'created_on', type => 11},
        {name => 'updated_on', type => 11},
    );
};

table {
    name 'notes';
    pk 'id';
    columns (
        {name => 'id', type => 4},
        {name => 'user_id', type => 4},
        {name => 'title', type => 12},
        {name => 'description', type => 12},
        {name => 'reference', type => 12},
        {name => 'deleted', type => 4},
        {name => 'created_on', type => 11},
        {name => 'updated_on', type => 11},
    );
};

table {
    name 'stocks';
    pk 'id';
    columns (
        {name => 'id', type => 4},
        {name => 'user_id', type => 4},
        {name => 'note_id', type => 4},
        {name => 'created_on', type => 11},
        {name => 'updated_on', type => 11},
    );
};

table {
    name 'tag_map';
    pk 'id';
    columns (
        {name => 'id', type => 4},
        {name => 'user_id', type => 4},
        {name => 'note_id', type => 4},
        {name => 'tag_id', type => 4},
        {name => 'locked', type => 4},
        {name => 'created_by', type => 4},
        {name => 'created_on', type => 11},
        {name => 'updated_by', type => 4},
        {name => 'updated_on', type => 11},
    );
};

table {
    name 'tags';
    pk 'id';
    columns (
        {name => 'id', type => 4},
        {name => 'name', type => 12},
        {name => 'created_on', type => 11},
        {name => 'updated_on', type => 11},
    );
};

table {
    name 'users';
    pk 'id';
    columns (
        {name => 'id', type => 4},
        {name => 'facebook_id', type => 12},
        {name => 'email', type => 12},
        {name => 'password', type => 12},
        {name => 'gender', type => 12},
        {name => 'birthday', type => 9},
        {name => 'last_logined_on', type => 11},
        {name => 'deleted', type => 4},
        {name => 'created_on', type => 11},
        {name => 'updated_on', type => 11},
    );
};

1;
