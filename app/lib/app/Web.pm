package app::Web;
use strict;
use warnings;
use utf8;
use parent qw/app Amon2::Web/;
use File::Spec;
use app::Web::DB::Users;

# dispatcher
use app::Web::Dispatcher;
sub dispatch {
    return (app::Web::Dispatcher->dispatch($_[0]) or die "response is not generated");
}

sub set_fb_user_id_to_session {
    my ($self) = @_;
    my $user = app::Web::DB::Users->facebook_user($self);
    $self->session->set('user_id' =>  $user->id);
}

sub request_access_token{
    my ($self) = @_;
    my $code = $self->req->parameters->{code};
    $self->fb->request_access_token($code);
}

sub user_id{
    my ($self) = @_;
    return $self->session->get('user_id');
}

sub logined{
    my ($self) = @_;
    return defined $self->user_id ? 1 : 0;
}

# load plugins
__PACKAGE__->load_plugins(
    'Web::FillInFormLite',
    'Web::JSON',
    '+app::Web::Plugin::Session',
);

# setup view
use app::Web::View;
{
    sub create_view {
        my $view = app::Web::View->make_instance(__PACKAGE__);
        no warnings 'redefine';
        *app::Web::create_view = sub { $view }; # Class cache.
        $view
    }
}

# for your security
__PACKAGE__->add_trigger(
    AFTER_DISPATCH => sub {
        my ( $c, $res ) = @_;

        # http://blogs.msdn.com/b/ie/archive/2008/07/02/ie8-security-part-v-comprehensive-protection.aspx
        $res->header( 'X-Content-Type-Options' => 'nosniff' );

        # http://blog.mozilla.com/security/2010/09/08/x-frame-options/
        $res->header( 'X-Frame-Options' => 'DENY' );

        # Cache control.
        $res->header( 'Cache-Control' => 'private' );
    },
);

1;
