package app::Alias;
use strict;
use warnings;
use utf8;

use Path::Tiny;
use Module::Load qw/ load_remote /;

our @MODELS;

sub import {
    my $caller = caller();

    @MODELS = (map { $_->basename =~ s/\.pm$//r } grep { $_->is_file } path('lib', 'app', 'Model')->children)
        if @MODELS == 0;

    load_remote($caller, "app::Model::$_") for @MODELS;
    for my $model (@MODELS) {
        no strict 'refs';
        *{"$caller\::$model"} = sub {
            return "app::Model::$model";
        };
    }
}

1;
