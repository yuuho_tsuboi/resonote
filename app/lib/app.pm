package app;
use strict;
use warnings;
use utf8;
our $VERSION='0.01';
use 5.008001;

use app::Core ();

use parent qw/Amon2/;

# Enable project local mode.
__PACKAGE__->make_local_context();

sub fb {
    app::Core::fb;
}

sub db {
    app::Core::db;
}

sub logger {
    app::Core::logger;
}

1;
__END__

=head1 NAME

app - app

=head1 DESCRIPTION

This is a main context class for app

=head1 AUTHOR

app authors.
