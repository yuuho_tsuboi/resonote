use strict;
use warnings;
use Test::More;


use app;
use app::Web;
use app::Web::View;
use app::Web::ViewFunctions;

use app::DB::Schema;
use app::Web::Dispatcher;


pass "All modules can load.";

done_testing;
