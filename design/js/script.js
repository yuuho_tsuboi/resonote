$(document).ready(function(){
	var MAINCOLOR        = "#0e90a9";
	var MAINCOLOR_LIGHT1 = "#96d4e0";
	var MAINCOLOR_LIGHT2 = "#f1fcfe";
	var MAINCOLOR_DARK1  = "#037388";
	var MAINCOLOR_DARK2  = "#025a6a";
	var SUBCOLOR         = "#ffc33f";
	var SUBCOLOR_LIGHT1  = "#ffd882";
	var SUBCOLOR_LIGHT2  = "#fffcf5";
	var SUBCOLOR_DARK1   = "#f3ad0c";
	var FACEBOOK_COLOR1  = "#3b5998";
	var FACEBOOK_COLOR2  = "#19294c";

	var SPEED = 200;

	$("#main-visual .btn").hover(function() {
        $(this).stop().animate(
        { "backgroundColor" : MAINCOLOR_DARK2 }, SPEED);
 
        $(this).find("i").stop().animate(
        {
            "left" : "80%",
            "opacity" : "1" }, SPEED);
    },function() {
        $(this).stop().animate(
        { "backgroundColor" : MAINCOLOR }, SPEED);
 
        $(this).find("i").stop().animate(
        {
            "left" : "130%",
            "opacity" : "0" }, SPEED);
    });
    $("#main-visual .btn.login").hover(function() {
        $(this).stop().animate(
        { "backgroundColor" : FACEBOOK_COLOR2 }, SPEED);
 
        $(this).find("i").stop().animate(
        {
            "left" : "80%",
            "opacity" : "1" }, SPEED);
    },function() {
        $(this).stop().animate(
        { "backgroundColor" : FACEBOOK_COLOR1 }, SPEED);
 
        $(this).find("i").stop().animate(
        {
            "left" : "130%",
            "opacity" : "0" }, SPEED);
    });

    $(".notelist-area .panel .panel-body").hover(function() {
		$(this).stop().animate(
		{ "backgroundColor" : MAINCOLOR_LIGHT1 }, SPEED);
	},function() {
		$(this).stop().animate(
		{ "backgroundColor" : MAINCOLOR_LIGHT2 }, SPEED);
	});
	$(".notelist-area .panel.my .panel-body").hover(function() {
		$(this).stop().animate(
		{ "backgroundColor" : SUBCOLOR_LIGHT1 } , SPEED);
	},function() {
		$(this).stop().animate(
		{ "backgroundColor" : SUBCOLOR_LIGHT2 }, SPEED);
	});

	$(".taglist .tag").hover(function(){
		$(this).stop().animate(
		{
			"top" : "-5px",
			"opacity" : "0.5" }, SPEED);
	},function() {
		$(this).stop().animate(
		{
			"top" : "0",
			"opacity" : "1" }, SPEED);
	});

});
